let imagenes = ['pieza1', 'pieza0' , 'pieza2'];

let puzzle = document.getElementById('puzzle');
let piezas = document.getElementById('piezas');
let terminado = imagenes.length;

/* Contenedor de las piezas del rompecabezas */
for (let i = 0; i < imagenes.length; i++) {
    let div = document.createElement('div');
    div.className = 'pieza';
    div.id = imagenes[i]; //Info de las piezas
    div.draggable = true;
    div.style.backgroundImage = `url("assets/img/${imagenes[i]}.png")`;
    piezas.appendChild(div);
}

/* Contenedor del puzzle donde se van a soltar las piezas*/

for (let i = 0; i < terminado; i++) {
    let div = document.createElement('div');
    let p = document.createElement('p');
    div.className = `pieza-puzzle`;
    p.className = `img${i}`;
    div.dataset.id = i; //Info de las cajas
    puzzle.appendChild(div);
    p.innerHTML = 'Arrastre y suelte la imágene aquí';
    div.appendChild(p);
}

/* Transferir información - Cuando se hace click en una pieza hasta que se la deja en el contenedor puzzle  */

piezas.addEventListener('dragstart', e => {
    e.dataTransfer.setData('id', e.target.id); //Transfiere el nombre de la imagen contenida en el array
});

puzzle.addEventListener('dragover', e => {
    e.preventDefault();
    e.target.classList.add('hover');
});

puzzle.addEventListener('dragleave', e => {
    e.target.classList.remove('hover'); //e.target hace referencia al contenedor de la pieza
});


puzzle.addEventListener('drop', e => {
    e.target.classList.remove('hover');

    let id = e.dataTransfer.getData('id'); //Trae el nombre de la pieza contenida en el array que seleccionó el usuario
    let numero = id[5];
       
    let parrafo = document.querySelector(`.img${numero}`);
    
    if (e.target.dataset.id === numero) {
        e.target.appendChild(document.getElementById(id));
        parrafo.remove();
    }
});

function reinicio() {
    window.location.reload();
}


