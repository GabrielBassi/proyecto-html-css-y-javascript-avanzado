let video = document.querySelector('video');
let playButton = document.getElementsByClassName("play-video");
let pauseButton = document.getElementsByClassName("pause-video");
let time = document.getElementById ("timeVideo");

function playVideo() {
    video.play();
}

function pauseVideo() {
    video.pause();
}

//Evento loadeddata para obtener los metadata del video y poder así obtener la duración. 
video.addEventListener("loadeddata",function(){
    document.getElementById('timeVideo').innerHTML=` Duracion video : ${(video.duration).toFixed("F")} s`;
 },true);